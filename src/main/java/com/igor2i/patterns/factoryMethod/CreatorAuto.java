package com.igor2i.patterns.factoryMethod;

public class CreatorAuto extends Creator {

    private CreatorAuto() {
    }

    static class CreateHolder {
        public static final CreatorAuto creatorAuto = new CreatorAuto();
        public static CreatorAuto getInstance(){
            return creatorAuto;
        }
    }

    @Override
    public Transport crate() {
        return new Auto();
    }
}
