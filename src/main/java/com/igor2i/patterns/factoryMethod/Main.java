package com.igor2i.patterns.factoryMethod;

import java.util.ArrayList;

/**
 * Created by igor2i on 07.03.17.
 */
public class Main {

    public static void main(String[] args) {

        ArrayList<Creator> creators = new ArrayList<Creator>();
        creators.add(CreatorAuto.CreateHolder.getInstance());
        creators.add(CreatorTrain.CreateTrainHolder.getInstance());

        for(Creator creator : creators){

            System.out.println(creator.crate().getClass());

        }

    }

}
