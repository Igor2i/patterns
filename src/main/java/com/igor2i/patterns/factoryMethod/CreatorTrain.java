package com.igor2i.patterns.factoryMethod;

public class CreatorTrain extends Creator{

    private CreatorTrain(){}

    public static class CreateTrainHolder{

        private static final CreatorTrain createTrain = new CreatorTrain();
        public static CreatorTrain getInstance(){
            return createTrain;
        }

    }

    public Transport crate() {
        return new Train();
    }
}
