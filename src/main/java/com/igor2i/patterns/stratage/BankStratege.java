package com.igor2i.patterns.stratage;

/**
 * Created by igor2i on 09.03.17.
 */
public class BankStratege extends SendStratege {

    @Override
    public void SendMoney(int sum) {
        System.out.println("Sum " + sum + " procent "+ getPercent());
    }

}
