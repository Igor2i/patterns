package com.igor2i.patterns.stratage;

/**
 * Created by igor2i on 09.03.17.
 */
public abstract class SendStratege {

    private int time;
    private int percent;

    abstract void SendMoney(int sum);

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}
