package com.igor2i.patterns.stratage;

/**
 * Created by igor2i on 09.03.17.
 */
public class Main {
    public static void main(String[] args) {
        Context context = new Context();

        SendStratege sendStratege = new BankStratege();
        sendStratege.setPercent(200);
        sendStratege.setTime(1);

        SendStratege sendStratege1 = new MailStratege();
        sendStratege1.setPercent(300);
        sendStratege1.setTime(1);

        SendStratege sendStratege2 = new WebMoneyStrategy();
        sendStratege2.setPercent(2);
        sendStratege2.setPercent(200);

        context.addStrategy(sendStratege);
        context.addStrategy(sendStratege1);
        context.addStrategy(sendStratege2);


        context.sendMoney(500);
    }

}
