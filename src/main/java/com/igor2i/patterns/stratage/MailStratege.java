package com.igor2i.patterns.stratage;

/**
 * Created by igor2i on 09.03.17.
 */
public class MailStratege extends SendStratege {

    public MailStratege() {
        setTime(3);
    }

    @Override
    public void SendMoney(int sum) {
        System.out.println("you sent summ=" + sum + " with percent=" + getPercent() + " and delay 3 days");
    }

}
