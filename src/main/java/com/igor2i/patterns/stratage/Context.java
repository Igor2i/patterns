package com.igor2i.patterns.stratage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by igor2i on 09.03.17.
 */
public class Context {
    private SendStratege curentStrategy;
    private List<SendStratege> availableSendStrateges = new ArrayList<>();

    public void sendMoney(int money){
        fineStratege();
        curentStrategy.SendMoney(money);
    }

    public void fineStratege(){

        SendStratege tempStrategy = new BankStratege();
        tempStrategy.setTime(200);
        tempStrategy.setPercent(100);

        for(SendStratege sendStratege : availableSendStrateges){
            if(sendStratege.getTime() > 4){
                continue;
            }
            if(sendStratege.getPercent() < tempStrategy.getPercent()){
                tempStrategy = sendStratege;
            }
        }

        curentStrategy = tempStrategy;


    }

    public void addStrategy(SendStratege stratege){
        availableSendStrateges.add(stratege);
    }
}
