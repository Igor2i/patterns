package com.igor2i.patterns.builder.windows;

/**
 * Created by igor2i on 07.03.17.
 */
public abstract class WindowBuilder {

    protected WoodWindow woodWindow;

    public void create(){
        woodWindow = new WoodWindow();
    }

    public WoodWindow getWoodWindow() {
        if(woodWindow != null) {
            return woodWindow;
        }else {
            return new WoodWindow();
        }
    }

    public abstract void buildModel();

    public abstract void buildWight();

    public abstract void buildHeight();
}
