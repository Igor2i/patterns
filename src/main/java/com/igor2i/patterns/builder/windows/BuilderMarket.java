package com.igor2i.patterns.builder.windows;

/**
 * Created by igor2i on 07.03.17.
 */
public class BuilderMarket extends WindowBuilder {


    @Override
    public void buildModel() {
        woodWindow.setModel("MarketWindow");
    }

    @Override
    public void buildWight() {
        woodWindow.setWeight(100);
    }

    @Override
    public void buildHeight() {
        woodWindow.setHeight(120);
    }
}
