package com.igor2i.patterns.builder.windows;

/**
 * Created by igor2i on 07.03.17.
 */
public class RussianBuilder extends WindowBuilder {
    @Override
    public void buildModel() {
        woodWindow.setModel("RussianWindow");
    }

    @Override
    public void buildWight() {
        woodWindow.setWeight(300);
    }

    @Override
    public void buildHeight() {
        woodWindow.setHeight(240);
    }
}
