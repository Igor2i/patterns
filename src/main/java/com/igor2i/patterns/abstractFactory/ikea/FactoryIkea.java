package com.igor2i.patterns.abstractFactory.ikea;

import com.igor2i.patterns.abstractFactory.Chair;
import com.igor2i.patterns.abstractFactory.MebelFactory;
import com.igor2i.patterns.abstractFactory.Sofa;
import com.igor2i.patterns.abstractFactory.Taburetka;

/**
 * Created by igor2i on 07.03.17.
 */
public class FactoryIkea extends MebelFactory {
    @Override
    public Chair createChair() {
        return new ChairIkea();
    }

    @Override
    public Sofa createFofa() {
        return new SofaIkea();
    }

    @Override
    public Taburetka creatorTaburetka() {
        return new TaburetkaIkea();
    }
}
