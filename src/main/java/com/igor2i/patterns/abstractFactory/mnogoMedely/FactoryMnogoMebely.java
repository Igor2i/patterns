package com.igor2i.patterns.abstractFactory.mnogoMedely;

import com.igor2i.patterns.abstractFactory.Chair;
import com.igor2i.patterns.abstractFactory.MebelFactory;
import com.igor2i.patterns.abstractFactory.Sofa;
import com.igor2i.patterns.abstractFactory.Taburetka;

/**
 * Created by igor2i on 07.03.17.
 */
public class FactoryMnogoMebely extends MebelFactory{
    @Override
    public Chair createChair() {
        return new ChairMnogoMebely();
    }

    @Override
    public Sofa createFofa() {
        return new SofaMnogoMebely();
    }

    @Override
    public Taburetka creatorTaburetka() {
        return new TaburetkaMnogoMebely();
    }
}
