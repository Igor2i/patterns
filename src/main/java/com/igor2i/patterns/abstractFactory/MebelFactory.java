package com.igor2i.patterns.abstractFactory;

/**
 * Created by igor2i on 07.03.17.
 */
public abstract class MebelFactory {

    public abstract Chair createChair();
    public abstract Sofa createFofa();

    public abstract Taburetka creatorTaburetka();

}
