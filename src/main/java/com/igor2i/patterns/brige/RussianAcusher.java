package com.igor2i.patterns.brige;

/**
 * Created by igor2i on 08.03.17.
 */
public class RussianAcusher extends Acusher {
    @Override
    public void healing() {

    }

    public RussianAcusher(Hospital hospital) {
        super(hospital);
    }
}
