package com.igor2i.patterns.brige;

/**
 * Created by igor2i on 08.03.17.
 */
public interface Hospital {
    void healing();
}
