package com.igor2i.patterns.brige;

/**
 * Created by igor2i on 08.03.17.
 */
public class Main {

    public static void main(String[] args) {

        Acusher acusher = new RussianAcusher(new Surgey());
        Acusher acusher1 = new RussianAcusher(new Genekologia());

        Acusher[] acushers = {acusher, acusher1};

        for (Acusher acusher2: acushers){
            acusher2.healing();
        }
    }
}
