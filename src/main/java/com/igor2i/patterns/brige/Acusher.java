package com.igor2i.patterns.brige;

/**
 * Created by igor2i on 08.03.17.
 */
public abstract class Acusher {
    private Hospital iHospital;

    Acusher(Hospital hospital) {
        this.iHospital = hospital;
    }

    public abstract void healing();
}
