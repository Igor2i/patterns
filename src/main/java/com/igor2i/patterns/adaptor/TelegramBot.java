package com.igor2i.patterns.adaptor;

/**
 * Created by igor2i on 08.03.17.
 */
public class TelegramBot {

    public void sendMessage(String msg, int GroupID, int UserID){
        System.out.println("You sent \""+ msg +"\" to user " + UserID + " and group " + GroupID);
    }

    public void sendSpame(int count, String msg){
        System.out.println("You sent spamm to people " + count);
    }

    public void sleep(int millis){
        System.out.println("Sleap to millis " + millis);
    }
}
