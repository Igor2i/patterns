package com.igor2i.patterns.adaptor;

/**
 * Created by igor2i on 08.03.17.
 */
public class AdaptorCompozitionVK implements Bot {

    private VKBot vkBot = new VKBot();

    private static final  int DELEY = 100;

    @Override
    public void sendMessage(String msg, int UserID) {
        vkBot.sendMessage(msg, UserID, checkIsFriend());
    }

    @Override
    public void sendSpame(String msg, int count) {
        vkBot.sendSpame(count,msg,DELEY);
    }

    @Override
    public void sleep(float millis) {

    }


    private boolean checkIsFriend(){
        return false;
    }
}
