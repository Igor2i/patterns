package com.igor2i.patterns.adaptor;

/**
 * Created by igor2i on 08.03.17.
 */
public interface Bot {

    void sendMessage(String msg, int UserID);

    void sendSpame(String msg, int count);

    void sleep(float millis);


}
