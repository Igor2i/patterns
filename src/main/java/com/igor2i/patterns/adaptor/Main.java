package com.igor2i.patterns.adaptor;

/**
 * Created by igor2i on 08.03.17.
 */
public class Main {

    public static void main(String[] args) {

        Bot bot = new AdapterGenireolizeTelegram();
        Bot bot1 = new AdaptorCompozitionVK();
        bot.sendMessage("hello friend",10);
        bot1.sendMessage("hello friend",10);

    }

}
