package com.igor2i.patterns.adaptor;

/**
 * Created by igor2i on 08.03.17.
 */
public class AdapterGenireolizeTelegram extends TelegramBot implements Bot{

    private int serchGroupID(){
        return 1;
    }

    @Override
    public void sendMessage(String msg, int UserID) {
        this.sendMessage(msg,serchGroupID(), UserID);
    }

    @Override
    public void sendSpame(String msg, int count) {
        this.sendSpame(count,msg);
    }

    @Override
    public void sleep(float millis) {
        this.sleep((int)millis);
    }
}
