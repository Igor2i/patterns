package com.igor2i.patterns.state;

/**
 * Created by igor2i on 09.03.17.
 */
public enum DocumentStateEnum {

    LOAD, PREPAIRAD, SIGNED_1, SEND, RECIVED, SIGNED_2, ACCEPT, SEND_APPRUVMENT, RECIVED_APPRUVMENT, ERROR_SIGNED, REJECTED

}
