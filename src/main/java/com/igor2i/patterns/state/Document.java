package com.igor2i.patterns.state;

/**
 * Created by igor2i on 09.03.17.
 */
public class Document {

    private DocumentStateEnum state;

    public void doAction(DocumentStateEnum state){
        switch (this.state){
            case LOAD:
                if(state == DocumentStateEnum.PREPAIRAD){
                    System.out.println("set Prepared");
                    this.state = state;
                }else {
                    System.out.println("Error");
                }
                break;
            case PREPAIRAD:
                if(state == DocumentStateEnum.SIGNED_1){
                    System.out.println("set SIGNED_1");
                    this.state = state;
                }else if(state == DocumentStateEnum.ERROR_SIGNED){
                    System.out.println("set error SIGNED_1");
                    this.state = state;
                }else {
                    System.out.println("Error");
                }
                break;
            case SIGNED_1:
                if(state == DocumentStateEnum.SEND){
                    System.out.println("set SEND");
                    this.state = state;
                }else {
                    System.out.println("Error");
                }
                break;
            case SEND:
                if(state == DocumentStateEnum.RECIVED){
                    System.out.println("set RECIVED");
                    this.state = state;
                }else {
                    System.out.println("Error");
                }
                break;
            case RECIVED:
                if(state == DocumentStateEnum.SIGNED_2){
                    System.out.println("set SIGNED_2");
                    this.state = state;
                }else if(state == DocumentStateEnum.ERROR_SIGNED) {
                    System.out.println("set error SIGNED_2");
                    this.state = state;
                }else if(state == DocumentStateEnum.REJECTED) {
                    System.out.println("set error REJECTED");
                    this.state = state;
                }else {
                    System.out.println("Error");
                }
                break;
            case SIGNED_2:
                if(state == DocumentStateEnum.ACCEPT){
                    System.out.println("set ACCEPT");
                    this.state = state;
                }else {
                    System.out.println("Error");
                }
                break;
            case ACCEPT:
                if(state == DocumentStateEnum.SEND_APPRUVMENT){
                    System.out.println("set SEND_APPRUVMENT");
                    this.state = state;
                }else {
                    System.out.println("Error");
                }
                break;
            case SEND_APPRUVMENT:
                if(state == DocumentStateEnum.RECIVED_APPRUVMENT){
                    System.out.println("set RECIVED_APPRUVMENT");
                    this.state = state;
                }else {
                    System.out.println("Error");
                }
                break;
            case RECIVED_APPRUVMENT:
                System.out.println("Archive");
                break;
        }
    }

}
