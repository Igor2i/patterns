package com.igor2i.patterns.mementoOrSnapshot;

/**
 * Created by igor2i on 09.03.17.
 */
public class Level {

    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Memento saveState(){
        return new Memento(state);
    }

    public void restoreState(Memento memento){
        this.state = memento.getState();
    }

}
