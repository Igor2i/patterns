package com.igor2i.patterns.mementoOrSnapshot;

/**
 * Created by igor2i on 09.03.17.
 */
public class Memento {
    private final String state;

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
