package com.igor2i.patterns.mementoOrSnapshot;

/**
 * Created by igor2i on 09.03.17.
 */
public class CareTaket {

    private Memento taker;

    public Memento getTaker() {
        return taker;
    }

    public void setTaker(Memento taker) {
        this.taker = taker;
    }
}
