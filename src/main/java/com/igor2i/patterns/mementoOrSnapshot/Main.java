package com.igor2i.patterns.mementoOrSnapshot;

/**
 * Created by igor2i on 09.03.17.
 */
public class Main {

    public static void main(String[] args) {

        Level level = new Level();
        level.setState("Level1");

        CareTaket taket = new CareTaket();
        taket.setTaker(level.saveState());
        level.setState("Level2");

        System.out.println(level.getState());
        level.restoreState(taket.getTaker());

        System.out.println(level.getState());

    }

}
