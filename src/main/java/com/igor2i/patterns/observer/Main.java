package com.igor2i.patterns.observer;

/**
 * Created by igor2i on 09.03.17.
 */
public class Main {

    public static void main(String[] args) {

        HR bill = new HR("Bill");
        HR sara = new HR("Sara");

        ReserchObserver vasya = new ReserchObserver(1);
        ReserchObserver nixon = new ReserchObserver(2);
        ReserchObserver misha = new ReserchObserver(3);
        ReserchObserver tramp = new ReserchObserver(4);
        ReserchObserver olya = new ReserchObserver(5);


        bill.addObserver(vasya);
        bill.addObserver(nixon);
        bill.addObserver(misha);

        sara.addObserver(misha);
        sara.addObserver(tramp);
        sara.addObserver(olya);

        bill.notifyAllObserver();
        sara.notifyAllObserver();
    }

}
