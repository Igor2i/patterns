package com.igor2i.patterns.observer;

/**
 * Created by igor2i on 09.03.17.
 */
public interface iObserable {

    void addObserver(iObserver iObserver);
    void removeObserver(iObserver iObserver);
    void notifyAllObserver();

}
