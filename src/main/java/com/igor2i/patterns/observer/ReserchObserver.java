package com.igor2i.patterns.observer;

/**
 * Created by igor2i on 09.03.17.
 */
public class ReserchObserver implements  iObserver {

    private int id;

    public ReserchObserver(int id) {
        this.id = id;
    }

    @Override
    public void message(String message) {
        System.out.println(message + " " + id);
    }
}
