package com.igor2i.patterns.observer;

/**
 * Created by igor2i on 09.03.17.
 */
public interface iObserver {
    void message(String message);

}
