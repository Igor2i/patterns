package com.igor2i.patterns.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by igor2i on 09.03.17.
 */
public class HR implements iObserable {

    private String name;

    private List<iObserver> iObservers = new ArrayList<>();

    public HR(String name) {
        this.name = name;
    }

    @Override
    public void addObserver(iObserver iObserver) {
        iObservers.add(iObserver);
    }

    @Override
    public void removeObserver(iObserver iObserver) {
        iObservers.remove(iObserver);
    }

    @Override
    public void notifyAllObserver() {
        iObservers.forEach(iObserver -> iObserver.message(name + " I'm found some job for you"));
    }
}
