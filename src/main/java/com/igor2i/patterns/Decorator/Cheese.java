package com.igor2i.patterns.Decorator;

/**
 * Created by igor2i on 08.03.17.
 */
public class Cheese extends PizzaComponent {

    public Cheese(PizzaComponent pizzaComponent) {
        super(pizzaComponent);
    }

    @Override
    public void showComponent() {
        super.showComponent();
        System.out.println("Cheese");
    }

}
