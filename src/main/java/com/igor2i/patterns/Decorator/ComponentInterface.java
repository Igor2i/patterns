package com.igor2i.patterns.Decorator;

/**
 * Created by igor2i on 08.03.17.
 */
public interface ComponentInterface {

    void showComponent();

}
