package com.igor2i.patterns.Decorator;

/**
 * Created by igor2i on 08.03.17.
 */
public class Mushuls extends PizzaComponent {

    public Mushuls(ComponentInterface componentInterface) {
        super(componentInterface);
    }

    @Override
    public void showComponent() {
        super.showComponent();
        System.out.println("Mushuls");
    }
}
