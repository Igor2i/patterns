package com.igor2i.patterns.Decorator;

/**
 * Created by igor2i on 08.03.17.
 */
public abstract class PizzaComponent implements ComponentInterface{

    protected PizzaComponent pizzaComponent;

    public PizzaComponent(ComponentInterface componentInterface){
//        this.pizzaComponent = componentInterface;
    }

    @Override
    public void showComponent() {
        pizzaComponent.showComponent();
    }
}
