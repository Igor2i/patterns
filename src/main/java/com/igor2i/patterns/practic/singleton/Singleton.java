package com.igor2i.patterns.practic.singleton;

/**
 * Created by igor2i on 20.03.17.
 */
public class Singleton {

    private static volatile Singleton instance;

    private Singleton(){}

    public static Singleton getInstance(){
        Singleton localInstance = instance;
        if(localInstance == null){
            synchronized (Singleton.class){
                localInstance = instance;
                if(localInstance == null){
                    instance = localInstance = new Singleton();
                }
            }
        }
        return localInstance;
    }
}
