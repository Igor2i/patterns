package com.igor2i.patterns.practic.singleton2;

/**
 * Created by igor2i on 22.03.17.
 */
public class Singleton {

    private static volatile Singleton instance;

    private Singleton() {}

    public static Singleton getInstance(){

        if(instance == null){
            synchronized (Singleton.class){
                if(instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
