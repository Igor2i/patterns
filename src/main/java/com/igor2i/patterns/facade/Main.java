package com.igor2i.patterns.facade;

/**
 * Created by igor2i on 08.03.17.
 */
public class Main {

    public static void main(String[] args) {

        Human human = new Human();
        human.life();
    }

}
