package com.igor2i.patterns.facade;

/**
 * Created by igor2i on 08.03.17.
 */
public class Human {

    private Brain brain;
    private Hands hands;
    private Heart heart;

    public Human(){

        brain = new Brain();
        hands = new Hands();
        heart = new Heart();

    }

    public void life(){
        brain.stateCold(100);
        brain.think("8 marta");

        hands.doWork();

        heart.stateHot(120);
        heart.takeRitm();
    }



}
