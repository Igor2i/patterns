package com.igor2i.patterns.facade;

/**
 * Created by igor2i on 08.03.17.
 */
public class Brain {

    void think(String musl){
        System.out.println("I think of "+ musl);
    }

    void stateCold(int temp){
        System.out.println("Brain temerature is " + temp );
    }

}
