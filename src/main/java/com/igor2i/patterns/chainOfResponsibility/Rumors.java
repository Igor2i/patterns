package com.igor2i.patterns.chainOfResponsibility;

/**
 * Created by igor2i on 09.03.17.
 */
public abstract class Rumors {

    protected Rumors rumors;

    public boolean isTrue;

    public Rumors setNext(Rumors rumors){
        this.rumors = rumors;
        return rumors;
    }

    abstract void writeRumors(String msg);

    public void chain(String msg){
        if(isTrue){
            writeRumors(msg);
            return;
        }

        if(this.rumors != null){
            this.rumors.chain(msg);
        }
    }

}
