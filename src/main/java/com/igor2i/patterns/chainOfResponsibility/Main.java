package com.igor2i.patterns.chainOfResponsibility;

/**
 * Created by igor2i on 09.03.17.
 */
public class Main {

    public static void main(String[] args) {

        BabkiRumors babkiRumors = new BabkiRumors();
        OfficeRumors officeRumors = new OfficeRumors();
        TeshaRumors teshaRumors = new TeshaRumors();

        babkiRumors.isTrue = true;
        officeRumors.isTrue = false;
        teshaRumors.isTrue = false;

        officeRumors.setNext(teshaRumors.setNext(babkiRumors));

        officeRumors.chain("I want work sb");

    }

}
