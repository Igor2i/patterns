package com.igor2i.patterns.chainOfResponsibility;

/**
 * Created by igor2i on 09.03.17.
 */
public class BabkiRumors extends Rumors {
    @Override
    void writeRumors(String msg) {
        System.out.println("Babki said " + msg);
    }
}
