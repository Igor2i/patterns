package com.igor2i.patterns.Composite;

/**
 * Created by igor2i on 08.03.17.
 */
public class Main {

    public static void main(String[] args) {

        Home[] homes = {new Home(3), new Home(2), new Home(2), new Home(4)};

        City[] cities = {new City(homes), new City(homes), new City(homes)};

        Province[] provinces = {new Province(cities), new Province(cities), new Province(cities)};

        Region[] regions = { new Region(provinces), new Region(provinces), new Region(provinces) };

        Country country = new Country(regions);

        System.out.println(country.count());

    }

}
