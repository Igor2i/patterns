package com.igor2i.patterns.Composite;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by igor2i on 08.03.17.
 */
public abstract class GroupComponent implements Component {

    private List<Component> list;

    public GroupComponent(Component[] components) {
        list = new LinkedList<>();
        for(Component c: components)
            list.add(c);
    }

    @Override
    public void add(Component c) {
        list.add(c);
    }

    @Override
    public long count() {

        int count = 0;
        for (Component c: list) {
            count += c.count();
        }
        return count;
    }

    @Override
    public long summMoney() {
        int count = 0;
        for (Component c: list) {
            count += c.count();
        }
        return count;
    }
}
