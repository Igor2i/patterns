package com.igor2i.patterns.Composite;

import java.util.Random;

/**
 * Created by igor2i on 08.03.17.
 */
public class Home implements Component, Copyable {

    private static Random random = new Random();

    private TypeSubject type;

    private int countHuman;

    public Home() {
        countHuman = random.nextInt(10);
    }

    public Home(int countHuman) {
        this.countHuman = countHuman;
    }

    @Override
    public void add(Component c) {
        throw new UnsupportedOperationException("Операция не поддерживается");
    }

    @Override
    public long count() {
        return countHuman;
    }

    @Override
    public long summMoney() {
        return 1L;
    }

    @Override
    public Copyable copy() {
        return null;
    }

    public void setType(TypeSubject type) {
        this.type = type;
    }
}
