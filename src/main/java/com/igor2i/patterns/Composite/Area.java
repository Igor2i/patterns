package com.igor2i.patterns.Composite;

import java.util.Random;

/**
 * Created by igor2i on 08.03.17.
 */
public class Area extends GroupComponent {

    private static Random random = new Random();

    private int summMoney;

    public Area(Component[] components) {
        super(components);
    }

    @Override
    public long summMoney() {
        return 1L;
    }
}
