package com.igor2i.patterns.Composite;

/**
 * Created by igor2i on 08.03.17.
 */
public enum TypeSubject {
    HUMAN, AREA, SUMM_MONEY
}
