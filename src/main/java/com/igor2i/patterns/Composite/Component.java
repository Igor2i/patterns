package com.igor2i.patterns.Composite;

/**
 * Created by igor2i on 08.03.17.
 */
public interface Component {

    void add(Component c);

    long count();

    long summMoney();

}
